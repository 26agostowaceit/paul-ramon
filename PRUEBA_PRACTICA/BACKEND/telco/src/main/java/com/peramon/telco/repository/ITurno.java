package com.peramon.telco.repository;

import com.peramon.telco.model.Turn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ITurno extends JpaRepository<Turn, Long>{

    // Query unused - not implemented
    @Query(value = "select * from turn where turn.userr = :user and turn.pass = :pass", nativeQuery = true)
    Turn findByUserPass(@Param("user") String user, @Param("pass") String pass);

    // Query by determining the last turn
    @Query(value = "SELECT * FROM turno ORDER BY id DESC LIMIT 1", nativeQuery = true)
    Turn findLastTurn();

}
