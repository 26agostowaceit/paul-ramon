package com.peramon.telco.controller;

import com.peramon.telco.model.Login;
import com.peramon.telco.model.Turn;
import com.peramon.telco.service.LoginService;
import com.peramon.telco.service.TurnoService;
import jakarta.websocket.server.PathParam;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins="*",methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping(value = "/turns")
public class TurnoController {

    private String token;
    
    @Autowired
    TurnoService turnController;

    @Autowired
    LoginService loginController;

    // EVERYTHING only works with the first login, review how to fix it
    @GetMapping("/{name}/{clave}")
    public Turn loginTurn(@PathParam(value = "name") String name, @PathParam(value = "clave") String clave) throws Exception {
        Turn turnToken = turnController.login(name, clave);
        token = turnToken.getToken();
        return turnToken;
    }


    // Get all turns for user view
    @GetMapping(value = "/all")
    public List<Turn> getAllturns() throws Exception {
        return turnController.getTurns(this.token);
    }

    // // Get turn by id - no usual method
    @GetMapping(value = "/turno/{id}")
    public Turn getTurnId(@PathVariable Long id) throws Exception {
        return turnController.getTurnId(id, this.token);
    }

    // Add new turns
    @PostMapping(value = "/add")
    public Turn addTurno(@RequestBody Turn nuevoTurn) throws Exception{
        loginController.updateListTurn(nuevoTurn);
        return nuevoTurn;
    }

    // Rest to get the last turn
    @GetMapping(value = "/last")
    public Turn lastTurn() {
        return turnController.lastTurn();
    }

    // Method to see the shifts with the login
    @GetMapping(value = "/login")
    public List<Login> getLogin(@RequestParam("username") String username, @RequestParam("pass") String pass){
        Login login = loginController.findById(1L);
        if(login.getUsername().equals(username) && login.getPass().equals(pass)){
            return loginController.getLogin();
        }
        return null;
    }

    // Method used in the window to add the comment and the file
    @PutMapping(value = "/window")
    public Turn lastTurnWindow(@RequestBody Turn turn) throws Exception {
        Turn lastTurn = turnController.lastTurn();
        lastTurn.setComment(turn.getComment());
        lastTurn.setContentFile(turn.getContentFile());
        return turnController.createTurn(lastTurn, this.token);
    }

        
}
