package com.peramon.telco.service;

import com.peramon.telco.model.Login;
import com.peramon.telco.model.Turn;
import com.peramon.telco.repository.ILogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginService {

    @Autowired
    ILogin loginRepository;

    // Update the list of turns inside the login entity
    public Login updateListTurn(Turn data){
        Login loginActual = loginRepository.findById(1L).orElse(null);
        if (loginActual == null){
            return loginActual;
        }
        loginActual.getTurnList().add(data);
        return loginRepository.save(loginActual);
    }

    // Obtain the login with the turns - method no used in the front
    public List<Login> getLogin(){
        return loginRepository.findAll();
    }

    // Obtain the login with the id - simulated the login of the application
    public Login findById(Long id){
        return  loginRepository.findById(id).orElse(null);
    }
}
