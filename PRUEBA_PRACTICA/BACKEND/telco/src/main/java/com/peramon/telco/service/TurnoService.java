package com.peramon.telco.service;

import java.util.List;
import java.util.UUID;

import com.peramon.telco.model.Turn;
import com.peramon.telco.repository.ITurno;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TurnoService {
    @Autowired
    ITurno turnRepository;

    
    public List<Turn> getTurns(String token) throws Exception{
//        if(!validTurno(token)){
//            throw new Exception("ERRO EN CONSULTA");
//        }
        return turnRepository.findAll();
    }
    
    public Turn getTurnId(Long id, String token) throws Exception{
        if(!validTurno(token)){
            throw new Exception("Query failed");
        }
        return turnRepository.findById(id).orElse(null);
    }
    
    public Turn createTurn(Turn nuevoTurn, String token) throws Exception{
//        if(!validTurno(token)){
//            throw new Exception("ERRO EN CONSULTA");
//        }
        return turnRepository.save(nuevoTurn);
    }

    public Turn lastTurn(){
        return turnRepository.findLastTurn();
    }
    
    
    
    // User validation
    public Turn login(String user, String pass) throws Exception{
        Turn turn = turnRepository.findByUserPass(user, pass);
        // log.info("USUARIO" + turn);

        if(turn == null){
            throw new Exception("User error");
        }
        
        turn.setUuid(UUID.randomUUID().toString());
        turnRepository.save(turn);
        turn.setToken(getToken(turn));
        return turn;
    }
    
   /* public void validateToken(){
        Turn turn = turnoRepository.findById(1L).get();
        String token = obtenerToken(turn);
        log.warn("token: "+token);
        boolean valid = validTurno(token);
        log.warn("valid: " + valid);
    }*/
    
    public String getToken(Turn turn){
        String data = turn.createString();
        return Base64.encodeBase64String(data.getBytes());
    }
    
    public boolean validTurno(String base64){
        String input = new String(Base64.decodeBase64(base64));
        Long id = Long.valueOf(input.split(",")[0]);
        Turn turn = turnRepository.findById(id).get();
        String valid = turn.createString();
        return input.equals(valid);
    }
     
}
