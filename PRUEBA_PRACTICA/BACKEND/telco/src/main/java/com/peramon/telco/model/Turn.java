package com.peramon.telco.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="turno")
public class Turn {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "area")
    private String area;
    private String proceduree;
    private String observation;
    private String comment;
    @Lob
    private byte[] contentFile;

    @Column(name = "date_created")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date_created;
            
    private String userr;
    private String pass;
    private String uuid;
    
    @Transient
    private String token;

    @PrePersist
    public void prePersist() {
        date_created = new Date();
    }

    public String createString(){
        return id+","+ userr +","+pass+","+uuid;
    }
}
