package com.peramon.telco.repository;

import com.peramon.telco.model.Login;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ILogin extends JpaRepository<Login, Long> {
}
