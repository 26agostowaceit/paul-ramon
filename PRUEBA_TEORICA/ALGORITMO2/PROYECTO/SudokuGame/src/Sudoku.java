public class Sudoku {
    
    private int[][] sudokuNumbers = new int[4][4];

    Sudoku(int[][] sudokuNumbers){
        this.sudokuNumbers = sudokuNumbers;
    }
    
    protected boolean isValidSudoku() {
        // Grid size
        int limit = this.sudokuNumbers.length;
        // subgrid size 2x2
        int subgridSize = 2;

        // Check rows and columns
        for (int i = 0; i < limit; i++) {
            boolean[] rowCheck = new boolean[limit];
            boolean[] colCheck = new boolean[limit];
            System.out.println();
            for (int j = 0; j < limit; j++) {
                int rowValue = this.sudokuNumbers[i][j];
                int colValue = this.sudokuNumbers[j][i];
                // Verify numbers
                if (rowValue < 1 || rowValue > limit || rowCheck[rowValue - 1]) {
                    return false;
                }
                rowCheck[rowValue - 1] = true;

                if (colValue < 1 || colValue > limit || colCheck[colValue - 1]) {
                    return false;
                }
                colCheck[colValue - 1] = true;
                System.out.print(this.sudokuNumbers[i][j]);
            }
        }

        // Verify subgrids
        for (int row = 0; row < limit; row += subgridSize) {
            for (int col = 0; col < limit; col += subgridSize) {
                boolean[] subgridCheck = new boolean[limit];

                for (int i = row; i < row + subgridSize; i++) {
                    for (int j = col; j < col + subgridSize; j++) {
                        int value = this.sudokuNumbers[i][j];

                        if (value < 1 || value > limit || subgridCheck[value - 1]) {
                            return false;
                        }
                        subgridCheck[value - 1] = true;
                    }
                }
            }
        }
        return true;
    }
}
