public class Main {
    public static void main(String[] args) {
        int[][] firstAttempt = {
                {1, 2, 4, 3},
                {3, 4, 2, 1},
                {2, 1, 3, 4},
                {4, 3, 1, 1}
        };

        int[][] secondAttempt = {
                {1, 2, 4, 3},
                {3, 4, 2, 1},
                {2, 1, 3, 4},
                {4, 3, 1, 2}
        };
        System.out.print("-------------- First Attempt ------------------");
        // Test 1
        Sudoku firstGame = new Sudoku(firstAttempt);
        boolean isValidFirstAttempt = firstGame.isValidSudoku();
        System.out.println("\nThe sudoku result is -> " + isValidFirstAttempt);

        System.out.print("\n-------------- Second Attempt ------------------");
        // Test 2
        Sudoku secondGame = new Sudoku(secondAttempt);
        boolean isValidSecondAttempt = secondGame.isValidSudoku();
        System.out.println("\nThe sudoku result is -> " + isValidSecondAttempt);

    }
}