# TURNS APLICATION WEB SERVICE SERVICE

This README was created for the exercise of the Telconet company, which is a small implementation of an API for shift management.

## BACKEND

### Tools needed

- JDK 17 or OpenJDK 17 onwards.
- PostgreSQL 12 onwards in this case version 15.3 has been used https://www.postgresql.org/download/
- Code editor (IntelliJ IDEA was used for this project)

### Proyect Configuration

The configuration of the project was done on the website of [Spring Initializr](https://start.spring.io/)
where the following configuration was made.

> `Proyect`-`Maven`
> `Language`-`Java` 
> `Versión Spring Boot`-`3.1.2`
> `Packaging`-`Jar`
> `Java Versión`-`17`

### Project Dependencies

```<dependencies>
	<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>
```

### Properties and BD

In the file `applications.properties` is the configuration of the
Spring Boot project, the following line specifies the port
where the application will be deployed.

```# local deployment port
server.port = 8080
```

#### Database Configuration

For the base, the only thing that was done was to create a new Database
In PostreSQL, the creation of tables and relationships is done by
JPA in Spring Boot, so that the project can connect to the database
PostgreSQL data you have to specify the **port**,
**name of the db**, **username** and the **password**
in the `applications.properties` file. Where it says `replace` you have to
put the configuration of your bd, the rest are annotations that we
allow you to create the database and view the SQL queries.

```

# Configuration for the db in PostgreSQL
# Port and name of the db
spring.datasource.url=jdbc:postgresql://localhost:replace/replace
spring.datasource.username=replace # Database User
spring.datasource.password=replace # Database password

spring.datasource.driver-class-name=org.postgresql.Driver
spring.jpa.hibernate.ddl-auto=create
spring.datasource.initialization-mode=always

# show the SQL queries
spring.jpa.properties.hibernate.format_sql=true

```
### APIS
The documentation for the use of each API generated in the controllers can be found at the following address 
can be found at the following address.

https://documenter.getpostman.com/view/7970730/2s9Y5Zugdq

## FRONTEND

### Angular configuration

- Angular 16
- Node 18
- npm 9

Update Node Module

```
npm install
```

Execute the angular proyect
```
ng serve
```